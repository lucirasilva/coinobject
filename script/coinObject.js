/*GLOBAL VARIABLES*/
const FLIP_PER_MILLISECONDS = 500;
let results = [];
let images = [];
/*GLOBAL VARIABLES*/

const createElement = (element, id, parent) => {
    let createdElement = document.createElement(element);
    createdElement.setAttribute('id', id);
    parent.appendChild(createdElement);
    return createdElement;
}

const conteiner = createElement('div', 'conteiner', document.body);
const img = createElement('img', 'winner', conteiner);
const h2 = createElement('h2', 'tailOrHead', conteiner);
const startButton = createElement('button', 'flipButton', conteiner);
startButton.innerText = 'Flip';

const resetVariables = () => {
    results = [];
    images = [];
}
const startFlip = () => {
    resetVariables();

    const flipInterval = setInterval(() => {
        if(results.length < 20){
            display20Flips();
            display20Images();
        }
        else {
            clearInterval(flipInterval);

        }
    }, FLIP_PER_MILLISECONDS);
}

startButton.addEventListener('click', startFlip);


const coin = {
    state: 0,
    sides: ['head', 'tail'],
    flip() {
        this.state = Math.floor(Math.random() * (1 - 0 + 1) + 0);
        return this.state;
    },
    toString() {
        let side = this.state;
        h2.innerText = this.sides[side];
        return this.sides[side];
    },
    toHTML() {
        let side = this.sides[this.state];
        img.src = `imgs/dog-${side}.jpg`;
        img.classList.add('winnerSide');
        img.alt = `dog ${side}`;
        return img;
    }
 };
 
 const display20Flips = () => {
    coin.flip();
    const side = coin.toString();
    results.push(side); 
    return results;
 }
 
 const display20Images = () => {
    images.push(coin.toHTML().alt);
    return images;
 }




